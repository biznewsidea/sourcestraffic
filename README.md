# README #

This README would normally document whatever steps are necessary to get your application up and running.
Learn Markdown
Suppose you want to have a party and you have invited all your friends. You are a vegetarian, but your friends are not like you. Of course, you want everyone to have fun at this party. So, you will make foods that everyone wants! Even in the simplest life events, we make decisions based on the knowledge we have of our audience. Because this way the work is easier and faster, and we have reached our goal.
In short, if we consider what the target customer is at the business level, we can say that the target customer is those who use your product. Your target customer is the person you know as the person who needs your product. Defining target customers can be a big part of your target market. Because you know the needs of the target market.
•	[How To Buy Traffic]( https://www.targetedwebtraffic.com/product-category/website-traffic/)
Decision making is important for target customers. This is because competition is so important in today's business and not all businesses can satisfy all customers.
One of the most important problems that companies have in marketing activities is the obsolescence of customer information, lower sales than expected and having a lot of referrals and many customer complaints. These problems indicate the weakness of marketing management in identifying target customers. The most important part of marketing is "customer perception", which means knowing the needs, feelings, tastes and behavior of customers.
What is the target market? Understanding and determining the role of the target market in digital marketing
How to identify target customers?
Now that you understand what the target customer is, the question arises, how can they be identified? Sometimes the best choice to get to know customers is to talk to them constantly. Businesses generally use more formal methods, which include:
- Focused groups
It is a type of qualitative research in which a group of people are asked about their perceptions, ideas, opinions and thoughts about a particular product, service, idea, advertisement or packaging. Of course, this method of data collection is exploratory and without additional statistical analysis, it is not possible to say to what extent the findings are true.
- Field research
The most common form of this method of data collection in marketing is the design of a questionnaire by the researcher for a sample of the target customer. The researcher makes hypotheses and asks questions to measure each of the variables examined in the hypothesis and sends a set of questionnaires for a sufficient number (statistical sample) of his target population. Assuming that the questionnaires are completely completed, in this way, a reliable picture of community opinions can be obtained.
- In-depth interview
One of the disadvantages of field research questionnaires is that the researcher is not very open in dealing with details and there is a high probability of bias in the answers. People may not express or justify their true feelings. For this reason, individual interviews are used. But the findings can hardly be generalized to society as a whole.
- Consumer buying behavior
Some other marketing behaviorists prefer to study consumer buying behavior. In this method, suggestions are made based on the findings and observations. For example, in stores where the goods are not arranged properly, customers move faster and cannot react positively to the signs and brands. In this way, customers leave the store.
[What is Customer Experience (CX) in Digital Marketing?]( https://www.targetedwebtraffic.com/how-to-create-a-digital-marketing-strategy-for-beginners-in-7-steps/)
- Mystery shopping
This method is used to provide real feedback to business executives. For example, going to a branch of a pizzeria and ordering food, keeping accurate records of events such as how long the waiter's visit to place an order, preparation time, quality of service, quality of food, price, and many other factors that may be important to customers. Have.
- Use of CRM software
Customer relationship management software sometimes means an Excel file containing several columns (name, occupation, contact number, [website]( https://www.targetedwebtraffic.com), email, age, etc.) that are provided by previous customers and will help to sell new products in the future. Preparing such a database and how to extract information from them is one of the existing techniques for customer recognition.


### Who do I talk to? ###

* Repo owner or admin
* Other community or [contact us](https://www.targetedwebtraffic.com/contact-us/)